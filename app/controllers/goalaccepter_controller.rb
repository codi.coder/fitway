class GoalaccepterController < ApplicationController
  def accept_goal
      goal_id = params[:goal_id]
      accept = params[:accept]
      
      if accept.to_i == 1
        goal = Goal.find_by_id(params[:goal_id])
        goal.update_attributes(:accept=>params[:accept])
        redirect_to :controller=>'goals', :action=>'index', notice: 'Goal successfully accepted.'
      elsif accept.to_i == 0
        goal = Goal.find_by_id(params[:goal_id])
        goal.update_attributes(:accept=>params[:accept])
        redirect_to :controller=>'goals', :action=>'index', notice: 'Goal successfully rejected.'
      else
        redirect_to :controller=>'goals', :action=>'index', notice: 'Please try again later--' + accept + "--"
      end
  end
end
