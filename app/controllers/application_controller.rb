class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate, :except => [:login, :update_goal, :create_social_login, :newuser, :signup_new_user]
  helper_method :current_user

  def authenticate
    if session[:user_id].nil?
      flash[:error] = 'Please login before proceed.'
      redirect_to :controller => 'auth', :action => 'login'
    end
  end
  
  def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

end
