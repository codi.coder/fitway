class UsersController < ApplicationController
  before_filter :authorize_user_self_or_admin, :only => [:edit, :update, :destroy]
  before_filter :authorize_admin, :only => [:new, :create]
  helper_method :user_self_or_admin?

  def authorize_user_self_or_admin
    if !user_self_or_admin? 
      flash[:error] = 'The action is only allowed for the user him/ herself.'
      redirect_to :back
    end
  end

  def user_self_or_admin?
    user_session = User.find(session[:user_id])
    user_param = User.find(params[:id])
    user_session and user_param and 
      ((user_session.id == user_param.id) or user_session.role==User::ROLE_ADMIN)
  end
  
  def authorize_admin
    if !is_admin?
      flash[:error] = 'The action is only allowed for admin.'
      redirect_to :back
    end
  end
  
  def is_admin?
    user_session = User.find(session[:user_id])
    user_session and user_session.role==User::ROLE_ADMIN
  end
  
  # GET /users
  # GET /users.json
  def index
    
    if params[:search]
      @users = User.search(params[:search])
    else
      @users = User.all(:order=>"first_name ASC")
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end
  
  def newuser
    @user = User.new
    
    respond_to do |format|
      format.html # newuser.html.erb
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
end
