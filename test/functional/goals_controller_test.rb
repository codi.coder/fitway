require 'test_helper'

class GoalsControllerTest < ActionController::TestCase
  setup do
    @goal = goals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:goals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create goal" do
    assert_difference('Goal.count') do
      post :create, goal: { accept: @goal.accept, amount_of_transaction: @goal.amount_of_transaction, challenger_id: @goal.challenger_id, charity_id: @goal.charity_id, confirm_to_charity: @goal.confirm_to_charity, date_of_transaction: @goal.date_of_transaction, dollars_pledge: @goal.dollars_pledge, goaltype_id: @goal.goaltype_id, name: @goal.name, num_of_steps: @goal.num_of_steps, num_of_steps_so_far: @goal.num_of_steps_so_far, user_id: @goal.user_id }
    end

    assert_redirected_to goal_path(assigns(:goal))
  end

  test "should show goal" do
    get :show, id: @goal
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @goal
    assert_response :success
  end

  test "should update goal" do
    put :update, id: @goal, goal: { accept: @goal.accept, amount_of_transaction: @goal.amount_of_transaction, challenger_id: @goal.challenger_id, charity_id: @goal.charity_id, confirm_to_charity: @goal.confirm_to_charity, date_of_transaction: @goal.date_of_transaction, dollars_pledge: @goal.dollars_pledge, goaltype_id: @goal.goaltype_id, name: @goal.name, num_of_steps: @goal.num_of_steps, num_of_steps_so_far: @goal.num_of_steps_so_far, user_id: @goal.user_id }
    assert_redirected_to goal_path(assigns(:goal))
  end

  test "should destroy goal" do
    assert_difference('Goal.count', -1) do
      delete :destroy, id: @goal
    end

    assert_redirected_to goals_path
  end
end
