fitway
======
Doctor recommends that we should walk 10,000 steps per day. FitWay is a social website where user can create goals to walk a certain number of steps. Community can challenge each other. We also believe people love to compete! So why not compete to do a healthy habit. Users using this web application can also track high scores to see their position in the community.
